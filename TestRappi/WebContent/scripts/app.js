var contextPath;

var testModule = angular.module('testRappi',['ui.bootstrap']);

function routeConfig($routeProvider) {
	$routeProvider.				
		when('/', {
			controller: matrizController,
			templateUrl: 'matriz.html'
		});
}

testModule.config(routeConfig);