//Servicios
testModule.factory('calcularMatriz', ['$http', '$q',
	  function($http, $q) {
	    return function(data) {
	    	
	      var delay = $q.defer();
	      
	      if(data != undefined){
	    	  
	    	  $http.get(contextPath + '/rest/matrizService/calcularMatriz/'+data).success(	    			  	    			  
						function(data, status, headers, config) {
		      		delay.resolve(data);
	          }).error(function(data, status, headers, config) {
	        	  delay.reject('Unable to fetch recipes');
	          });
	    	  
	      }else{
	    	  delay.resolve();
	      }
	      
          return delay.promise;
       };
	}]);
